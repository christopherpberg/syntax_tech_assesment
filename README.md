## Deploy Django + Redis Devops stack to EC2
- Provisions an Ubuntu 18.04 machine with the minimal CPU and memory configuration on Ec2 (tobe able to use the free-tier subscription).  The hostname of this VM must be: syn-app01
- Load Balancer-443:8000
- Django + Redis Container-:8000
- Postgresql-:5432
- Provisions a PostgreSQL database (using Amazon RDS)
- Provision a load balancer which forwards the HTTPS traffic to the application server on port 8000.
## Setup

```
ansible-galaxy install -r roles/requirements.yml -f
```

## Server setup

```
ansible-playbook -b -k -K -i inventories/hosts_aws.ini syntax_tech_assesment.yml --extra-vars "aws_access_key=${AWS_ACCESS_KEY} aws_secret_key=${AWS_SECRET_KEY} aws_region=us-east-2" --ssh-extra-args='-o StrictHostKeyChecking=no'
```
